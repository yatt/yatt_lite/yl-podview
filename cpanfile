# -*- mode: perl; coding: utf-8 -*-

conflicts 'YATT';
requires  'YATT::Lite' => '>= 0.100';
requires 'Plack' => 0;
requires 'Pod::Simple::SimpleTree' => 0;

requires 'Locale::PO' => 0;

requires 'DBI' => 0;
requires 'DBIx::Class' => 0;
requires 'Otogiri' => 0;
